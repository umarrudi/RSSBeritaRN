import React, { Component } from 'react';
import { FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import { ListItem } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import DetailNewsView from './DetailNewsView'


class ViewBerita extends Component {

  state={
    data: [],
    page: 0,
    loading: false,
    refresh: false
  }

  componentWillMount() {
    this.fetchData();
  }

  fetchData = async() => {
    this.setState({ loading: true, loadingHeight: 80, refresh: true });
    const response = await fetch('https://api.rss2json.com/v1/api.json?rss_url=http%3A%2F%2Frss.detik.com%2Findex.php%2Fsport%2F'); 
    const json = await response.json();
    this.setState(state => ({
        data: json.items, 
        loading: false,
        loadingHeight: 0,
        refresh: false
    }));
  }

  handleEnd = () => {
    this.setState(state => ({ page: state.page + 1 })), 
    this.fetchData();
  }


  showDetail(indexDetail) {
    console.log(indexDetail);   
    //<DetailNewsView detail={indexDetail}/>
    Actions.detailNewsView({detail: indexDetail})
  }
  
  render() {
    return (
        <FlatList          
          data={this.state.data}
          keyExtractor={(x, i) => i}
          onEndReached={() => this.handleEnd()}
          onEndReachedThreshold={0}
          refreshing={this.state.refresh}
          onRefresh={() => this.fetchData()}
          renderItem={({ item }) => 
            <ListItem
              onPress={() => this.showDetail(item.link)}
              style={styles.listStyle} 
              avatarStyle={{ height: 50, width: 50 }}
              avatar={{ uri: item.enclosure.link }}
              title={`${item.title}`}
              subtitle={`${item.pubDate}`}
            />           
          }
        />
    );
  }
}

const styles = StyleSheet.create({
  listStyle: {
    height: 25
  }
});

export default ViewBerita;

